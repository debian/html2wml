#!/usr/bin/perl 

# This script will check if the Perl software installed on the system 
# has the necessary requirements in order to run the program. 
# 
# It works with versions of Perl as old as 4.036. 
# 
# Copyright (c)2000, 2001 Sebastien Aperghis-Tramoni


# [ configurable part ]

$needperl = 5.004;
$program  = 'Html2Wml';
@files = ('html2wml.cgi');

# [ end configuration ]


sub fatal { print @_; exit -1; }
sub nonfatal { print @_; }

## Check perl version
$] =~ /(\d+(\.\d+))/;
if($1 < $needperl) {
    &fatal("Your version of Perl ($1) is too old. \n", 
           "Please upgrade to version $needperl or later. \n");
}

## Search for needed modules
for $file (@files) {
    open(FILE, $file) 
      || &fatal("Can't open program file. Check if your archive is correct. \n");
    
    %modules = ();
    
    while(<FILE>) {
        chop;
        
        ## end of code section
        if(/^__END__/) { last }
        if(/^__DATA__/) { last }
        
        ## skip POD sections
        if(/^=head/) {
            do { $_ = <FILE> } until(/^=cut/)
        }
        
        if(/^\s*(use|require) +([\w:]+)/) { $modules{$2} = $1 }
    }
    
    close(FILE);
}

## Check if the modules are available
for $module (keys %modules) {
    eval "$modules{$module} $module";
    if($@) {
        if($modules{$module} eq 'use') {
            &fatal("Perl module $module is needed to use $program. \n")
        } else {
            &nonfatal("Perl module $module is not installed. \n", 
                      "Some fonctionalities of Html2Wml won't be available. \n")
        }
    }
}

exit 0;


#!/usr/bin/perl
use strict;
my $crlf = "\015\012";

print "Content-Type: text/html$crlf$crlf", 
      "<html>\n<body>\n", 
      "<p><b>Required Modules</b><br>\n";

for my $module (qw(strict CGI File::Basename Getopt::Long HTML::Parser 
                   LWP::UserAgent POSIX Text::Template URI URI::URL)) {
    print "checking for <b>$module</b>... ";
    eval "use $module";
    print $@ ? substr($@,0,index($@," at ")) : "ok";
    print "<br>\n";
}

print "</p>\n<p><b>Optional Modules</b><br>\n";

for my $module (qw(XML::Parser XML::LibXML XML::Checker::Parser)) {
    print "checking for <b>$module</b>... ";
    eval "use $module";
    print $@ ? substr($@,0,index($@," at ")) : "ok";
    print "<br>\n";
}

print "</p>\n</body>\n</html>\n";

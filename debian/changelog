html2wml (0.4.11+dfsg-3) UNRELEASED; urgency=medium

  * Use secure URI in Homepage field.
  * Fix day-of-week for changelog entry 0.4.11-1.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 02 Nov 2022 10:13:51 -0000

html2wml (0.4.11+dfsg-2) unstable; urgency=medium

  * Update Vcs-* headers for the move to Salsa.
  * Bump debhelper compatibility level to 13.
    + Build-depend on "debhelper-compat (= 13)" to replace debian/compat.
  * Change Homepage header to http://htmlwml.sourceforge.net/. (Previous
    homepage has fallen into the hands of a domain grabber.)
  * Declare compliance with Debian Policy 4.5.1.
  * Fix debian/copyright by splitting up stanzas for Artistic and GPL-2+.
    Fixes lintian warning missing-license-paragraph-in-dep5-copyright.
  * Remove trailing whitespace in old debian/changelog entry.
  * Set "Rules-Requires-Root: binary-targets". (dh_installdocs fails if
    set to "no".)
  * fix-spelling-errors.patch:
    + Include a fix for another typo found by Lintian.
    + Fix DEP3 Forwarded header.

 -- Axel Beckert <abe@debian.org>  Sat, 02 Jan 2021 10:07:08 +0100

html2wml (0.4.11+dfsg-1) unstable; urgency=medium

  * Add myself as co-maintainer.
  * Apply wrap-and-sort.
  * Add dependency on ${misc:Depends} for debhelper.
  * Bump debhelper compatibility to 9.
    + Update versioned build-dependency on debhelper accordingly.
  * Revamp debian/rules:
    + Replace "dh_clean -k" with "dh_prep"
    + Don't ignore all "make clean" errors, just if Makefile doesn't exist.
    + Add missing build-{arch,indep} targets.
    + Drop now redundant dh_installchangelogs parameter.
    + Drop manual removal of stamp files: done by dh_clean now.
    + Use dh_auto_{build,install,clean} instead of according $(MAKE) calls.
    + Remove now obsolete manual checking of DEB_BUILD_OPTIONS.
    + Finally switch to a minimal dh v7 style debian/rules file.
  * Add a Homepage header.
  * Add debian/watch file.
  * Switch to source format "3.0 (quilt)".
  * Cleanup Makefile patch to minimise the differences.
    + Put comments into patch description instead into the patched file.
    + Remove clean alias for uninstall target -- not needed and allows us
      to remove override for dh_auto_clean in debian/rules.
  * Declare compliance with Debian Policy 3.9.8.
  * Add patch to fix spelling errors found by lintian in man page.
  * Only install selected files from t/ directory via debian/docs. Most of
    them are already included in the upstream install target. Avoids
    installing an unneeded Makefile as documentation.
  * Add patch to t/Makefile to normalize examples directory naming and
    structure in /usr/share/doc/html2wml.
  * Add patch to remove potential (and unneeded) privacy breaches from
    documentation and examples.
  * Create packaging Git repository under collab-maint and create
    according Vcs-* headers.
  * Don't install README (only package description and license) and
    readme.* (same contents as man page) anymore. Also fixes Lintian
    warning possible-documentation-but-no-doc-base-registration.
  * Downgrade "httpd | httpd-cgi" from Recommends to Suggests (so that it
    doesn't pull in a web server by default), reverse the order and add an
    "Enhances: httpd-cgi" instead.
  * Remove non-free files t/wml1*.dtd by repacking the upstream tar ball.
  * Convert debian/copyright to machine-readable DEP5 format.
    + Also update copyright years and packaging copyright holders.
    + List non-free files in Files-Excluded header.

 -- Axel Beckert <abe@debian.org>  Sat, 10 Sep 2016 13:33:05 +0200

html2wml (0.4.11-1.2) unstable; urgency=medium

  * Non-maintainer upload
  * Swap binary-* targets to fix FTBFS with "dpkg-buildpackage -A". Patch
    by Santiago Vila. (Closes: #831931)
  * Add missing dependency on libcgi-pm-perl. (Closes: #835623)

 -- Axel Beckert <abe@debian.org>  Sat, 27 Aug 2016 19:50:03 +0200

html2wml (0.4.11-1.1) unstable; urgency=high

  * Non-maintainer upload
  * Replace dependency on "perl5" with just "perl". (Closes: #808189)

 -- Axel Beckert <abe@debian.org>  Sat, 19 Dec 2015 00:36:01 +0100

html2wml (0.4.11-1) unstable; urgency=low

  * New upstream release
  * Standards-Version: bumped to 3.7.2. No changes.
  * Suggests: iceweasel and wapua have been added
  * Description: iceweasel WAP plugin and wapua as a means to view
    the results of html2wml are now mentioned
  * Build-Depends: debhelper has been bumped to 5 and greater
  * DESTDIR has been added to clean target in rules file, to avoid a
    build error
  * moved DH_COMPAT from rules to compat
  * polished copyright file: author(s) -> author
  * download address changed in copyright file

 -- Werner Heuser <wehe@debian.org>  Fri, 09 Jan 2009 13:08:10 +0100

html2wml (0.4.10b5-1.2) unstable; urgency=low

  * NMU at the Munich Bugsquasing Party
  * dropped Recommends: viewml, since that was removed (Closes: #240561)

 -- Erich Schubert <erich@debian.org>  Sun, 18 Apr 2004 13:28:12 +0200

html2wml (0.4.10b5-1.1) unstable; urgency=low

  * NMU (patch available in the BTS)
  * Dropped dependency on non-free wap-wml-tools package from
    Recommends: to Suggests:. The only tool used from that package
    seems to be wmlc which is not strictly necessary and not used by
    default. (Closes: Bug#216563)

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 28 Dec 2003 11:45:08 +0100

html2wml (0.4.10b5-1) unstable; urgency=low

  * new upstream release
  * added man page, which was silently gone
  * modified package description to show new features
  * modified Makefile and rules to include the main script
    and the CGI finally, thanks to Kalle Valo
    <Kalle.Valo@hut.fi> (closes: Bug#162404).

 -- Werner Heuser <wehe@debian.org>  Wed, 27 Nov 2002 10:19:30 -0100

html2wml (0.4.9-1) unstable; urgency=low

  * New upstream release
  * modified target check-config to clean Build-Depends, thanks to
    Matt Taggart taggart@debian.org (closes: Bug#133743).

 -- Werner Heuser <wehe@debian.org>  Tue, 25 Jun 2002 10:18:41 -0100

html2wml (0.3.9-2) unstable; urgency=low

  * removed some nasty control chars from dsc and changes file

 -- Werner Heuser <wehe@debian.org>  Thu, 18 Jan 2001 09:22:02 -0100

html2wml (0.3.9-1) unstable; urgency=low

  * Initial Release.

 -- Werner Heuser <wehe@snafu.de>  Mon,  8 Jan 2001 14:07:10 -0100
